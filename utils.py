import re

import nltk
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from nltk.stem import LancasterStemmer, PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS


def perform_basic_analysis(dataset):
    """
    Calculates and plots the number of snippets for each programming language.
    """
    dt = dataset.groupby(['language'])['file_body'].count()
    dt.columns = ['language', 'num_snippets']
    dt.plot(kind = 'bar', x = 'language', y = 'count')
    plt.xlabel('programming language')
    plt.ylabel('number')
    plt.title('Number of code snippets per language')
    plt.show()
    
    return dt

def clean_snippet(snippet):
    """
    Cleans code snippets by removing numeric characters and other symbols.
    Performs word tokenization and word stemming. Removes english stop words.
    Possible extension: replacing numeric characters with a special token.
    """
    clean_snippet = re.sub(r'[^a-zA-Z#++.-]', ' ', str(snippet)).lower()

    porter = PorterStemmer()
    words = word_tokenize(clean_snippet)
    meaningful_clean_words = [porter.stem(w) for w in words if not w in ENGLISH_STOP_WORDS]
    return " ".join(meaningful_clean_words)

def read_dataset(file_path):
    """
    Reads the dataset and applies cleaning methods
    """
    dataset = pd.read_csv(file_path)
    dataset.file_body = dataset.file_body.apply(clean_snippet)
    return dataset
